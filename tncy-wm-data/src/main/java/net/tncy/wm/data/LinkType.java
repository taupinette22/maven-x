package net.tncy.wm.data;

/**
 *
 * @author x-roy
 */
public enum LinkType {
    
    ADSL, SDSL_ATM, SDSL_EFM, OPTICAL_FIBER, DARK_FIBER, LEASED_LINE, MICROWAVE_TRANSMISSION, LTE, UMTS;
    
}
