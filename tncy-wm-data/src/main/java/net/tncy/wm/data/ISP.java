package net.tncy.wm.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * ISP : Internet Service Provider
 *
 * @author x-roy
 */
@Entity
@Table(
        name = "ISP",
        uniqueConstraints = {
            @UniqueConstraint(name = "ISP_UNIQUE_NAME", columnNames = "NAME")
        }
)
@NamedQueries({
    @NamedQuery(name = ISP.FIND_ALL, query = "SELECT P FROM ISP p"),
    @NamedQuery(name = ISP.FIND_BY_NAME, query = "SELECT P FROM ISP p WHERE p.name LIKE :name")
})
@XmlRootElement
public class ISP {

     public static final String FIND_ALL = "ISP.findAll";

    public static final String FIND_BY_NAME = "ISP.findByName";
    
    @Id
//    @GeneratedValue()
    private Integer id;

    @NotNull
    private String name;

    public ISP() {
    }

    public ISP(String name) {
        this();
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    

}
