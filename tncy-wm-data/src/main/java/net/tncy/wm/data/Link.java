package net.tncy.wm.data;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author x-roy
 */
@Entity
@Table(
        name = "LINKS",
        uniqueConstraints = {
            @UniqueConstraint(name = "LINK_UNIQUE_NAME", columnNames = "NAME")
        }
)
@NamedQueries({
    @NamedQuery(name = Link.FIND_ALL, query = "SELECT l FROM Link l"),
    @NamedQuery(name = Link.FIND_BY_NAME, query = "SELECT l FROM Link l WHERE l.name LIKE :name")
})
@XmlRootElement
public class Link {
    
    public static final String FIND_ALL = "Link.findAll";

    public static final String FIND_BY_NAME = "Link.findByName";

    @Id
//    @GeneratedValue
    private Integer id;

    @NotNull
    private String name;

    private String providerLinkId;

    @Enumerated(EnumType.STRING)
    @NotNull
    private LinkType type;

    @ManyToOne(fetch = FetchType.LAZY)
    private ISP provider;

    @ManyToOne(fetch = FetchType.LAZY)
    private ISP carrier;

    @Temporal(TemporalType.DATE)
    private Date constructionDate;

    @Temporal(TemporalType.DATE)
    private Date creationDate;

    @Temporal(TemporalType.DATE)
    private Date resignationDate;

    @Temporal(TemporalType.DATE)
    private Date terminationDate;

    public Link() {
    }

    public Link(String label, LinkType type, ISP provider) {
        this.name = label;
        this.type = type;
        this.provider = provider;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return name;
    }

    public void setLabel(String label) {
        this.name = label;
    }

    public String getProviderLinkId() {
        return providerLinkId;
    }

    public void setProviderLinkId(String providerLinkId) {
        this.providerLinkId = providerLinkId;
    }

    public LinkType getType() {
        return type;
    }

    public void setType(LinkType type) {
        this.type = type;
    }

    public ISP getProvider() {
        return provider;
    }

    public void setProvider(ISP provider) {
        this.provider = provider;
    }

    public ISP getCarrier() {
        return carrier;
    }

    public void setCarrier(ISP carrier) {
        this.carrier = carrier;
    }

    public Date getConstructionDate() {
        return constructionDate;
    }

    public void setConstructionDate(Date constructionDate) {
        this.constructionDate = constructionDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getResignationDate() {
        return resignationDate;
    }

    public void setResignationDate(Date resignationDate) {
        this.resignationDate = resignationDate;
    }

    public Date getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(Date terminationDate) {
        this.terminationDate = terminationDate;
    }

}
