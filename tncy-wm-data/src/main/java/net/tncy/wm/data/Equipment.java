package net.tncy.wm.data;

import net.tncy.validator.constraints.devices.IMEI;

/**
 *
 * @author x-roy
 */
public class Equipment {

	private String manufacturer;

	private String model;

	@IMEI
	private String imei;

	private String mac;

	public Equipment() {
	}

	public Equipment(String manufacturer, String model, String imei, String mac) {
		this.manufacturer = manufacturer;
		this.model = model;
		this.imei = imei;
		this.mac = mac;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

}
