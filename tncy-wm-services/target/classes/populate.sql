# Populates ISP table
INSERT INTO ISP(ID, NAME) values (1, 'Orange')
INSERT INTO ISP(ID, NAME) values (2, 'SFR')
INSERT INTO ISP(ID, NAME) values (3, 'Bouygues Telecom')
INSERT INTO ISP(ID, NAME) values (4, 'Free')
INSERT INTO ISP(ID, NAME) values (5, 'Coriolis')
INSERT INTO ISP(ID, NAME) values (6, 'Nerim')
INSERT INTO ISP(ID, NAME) values (7, 'SANEF Telecom')

