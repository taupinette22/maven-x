package net.tncy.wm.services;


import java.net.URI;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import net.tncy.wm.data.ISP;
/**
 *
 * @author x-roy
 */
@Path("/providers")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Stateless
public class ISPMgtService extends AbstractRESTFulService<ISP> {

    @PersistenceContext(unitName = "wmPU")
    private EntityManager em;

    @Context
    private UriInfo uriInfo;

    public ISPMgtService() {
        super(ISP.class);
    }

    @POST
    @Override
    public Response create(ISP provider) {
        if (provider == null) {
            throw new BadRequestException();
        }
        persist(provider);
        URI providerUri = getUriInfo().getAbsolutePathBuilder().path(String.valueOf(provider.getId())).build();
        return Response.created(providerUri).build();
    }

    @PUT
    @Override
    public Response update(ISP provider) {
        if (provider == null) {
            throw new BadRequestException();
        }
        merge(provider);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @Override
    public Response delete(@PathParam("id") String id) {
        ISP provider = findById(new Integer(id));
        if (provider == null) {
            throw new NotFoundException();
        }
        remove(provider);
        return Response.noContent().build();
    }

    @GET
    @Path("{id :\\d+}")
    @Override
    public ISP get(@PathParam("id") String id) {
        ISP provider = findById(new Integer(id));
        if (provider == null) {
            throw new NotFoundException();
        }
        return provider;
    }

    @GET
    @Override
    public List<ISP> getAll() {
        List<ISP> list = findAll();
        return list;
    }
    
    @GET
    @Path("find")
    public List<ISP> findByName(@QueryParam("name") String name) {
        List<ISP> list = findByNamedQuery(ISP.FIND_BY_NAME, name + "%");
        return list;
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    @Override
    public String getCount() {
        return String.valueOf(count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected UriInfo getUriInfo() {
        return uriInfo;
    }

}
