package net.tncy.wm.services;


import java.net.URI;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import net.tncy.wm.data.Link;
/**
 *
 * @author x-roy
 */
@Path("/links")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Stateless
public class LinkMgtService extends AbstractRESTFulService<Link> {

    @PersistenceContext(unitName = "wmPU")
    private EntityManager em;

    @Context
    private UriInfo uriInfo;

    public LinkMgtService() {
        super(Link.class);
    }

    @POST
    @Override
    public Response create(Link link) {
        if (link == null) {
            throw new BadRequestException();
        }
        persist(link);
        URI linkUri = getUriInfo().getAbsolutePathBuilder().path(String.valueOf(link.getId())).build();
        return Response.created(linkUri).build();
    }

    @PUT
    @Override
    public Response update(Link link) {
        if (link == null) {
            throw new BadRequestException();
        }
        merge(link);
        return Response.ok().build();
    }

    @DELETE
    @Path("{id}")
    @Override
    public Response delete(@PathParam("id") String id) {
        Link link = findById(new Integer(id));
        if (link == null) {
            throw new NotFoundException();
        }
        remove(link);
        return Response.noContent().build();
    }

    @GET
    @Path("{id :\\d+}")
    @Override
    public Link get(@PathParam("id") String id) {
        Link link = findById(new Integer(id));
        if (link == null) {
            throw new NotFoundException();
        }
        return link;
    }

    @GET
    @Override
    public List<Link> getAll() {
        List<Link> list = findAll();
        return list;
    }
    
    @GET
    @Path("find")
    public List<Link> findByName(@QueryParam("name") String name) {
        List<Link> list = findByNamedQuery(Link.FIND_BY_NAME, name + "%");
        return list;
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    @Override
    public String getCount() {
        return String.valueOf(count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected UriInfo getUriInfo() {
        return uriInfo;
    }

}
