package net.tncy.wm.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author x-roy
 * @param <T>
 */
public abstract class AbstractRESTFulService<T> {

    public final Class<T> entityClass;

    protected AbstractRESTFulService(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public abstract Response create(T resource);

    public abstract Response update(T resource);

    public abstract Response delete(String id);

    public abstract T get(String id);

    public abstract List<T> getAll();

    public abstract String getCount();

    protected void persist(T entity) {
        getEntityManager().persist(entity);
    }

    protected void merge(T entity) {
        getEntityManager().merge(entity);
    }

    protected void remove(T entity) {
        getEntityManager().remove(entity);
    }

    protected T findById(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    protected List<T> findAll() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    protected List<T> findRange(int startIdx, int eltCount) {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(eltCount);
        q.setFirstResult(startIdx);
        return q.getResultList();
    }

    protected List<T> findByNamedQuery(String queryName, Object... params) {
        TypedQuery<T> query = getEntityManager().createNamedQuery(queryName, entityClass);
        int pos = 1;
        for (Object param : params) {
            query.setParameter(pos++, param);
        }
        return query.getResultList();
    }
    
    protected T findOneByNamedQuery(String queryName, Object... params) {
        TypedQuery<T> query = getEntityManager().createNamedQuery(queryName, entityClass).setMaxResults(1);
        int pos = 1;
        for (Object param : params) {
            query.setParameter(pos++, param);
        }
        return query.getResultList().get(0);
    }
    
    protected int count() {
        CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    protected abstract EntityManager getEntityManager();

    protected abstract UriInfo getUriInfo();

}
