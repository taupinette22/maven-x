package net.tncy.wm.application;

import java.util.logging.Logger;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author x-roy
 */
@Provider
@PreMatching
public class CORSResponseFilter implements ContainerResponseFilter {
    
    private final static Logger log = Logger.getLogger( CORSResponseFilter.class.getName() );

    @Override
    public void filter(ContainerRequestContext crc, ContainerResponseContext crc1) {
        log.info( "Executing REST response filter" );
        crc1.getHeaders().add("Access-Control-Allow-Origin", "http://localhost:8383");
        crc1.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
        crc1.getHeaders().add("Access-Control-Allow-Credentials", "true");
        crc1.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        crc1.getHeaders().add("Access-Control-Max-Age", "1209600");
    }

}
