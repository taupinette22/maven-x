package net.tncy.wm.application;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;
import net.tncy.wm.services.ISPMgtService;
import net.tncy.wm.services.LinkMgtService;
import org.eclipse.persistence.jaxb.rs.MOXyJsonProvider;

@javax.ws.rs.ApplicationPath("rs")
public class ApplicationConfig extends Application {

    private final Set<Class<?>> classes;

    public ApplicationConfig() {
        HashSet<Class<?>> c = new HashSet<>();
        c.add(CORSResponseFilter.class);
        c.add(CORSRequestFilter.class);
//        c.add(MoxyJsonFeature.class);
        c.add(MOXyJsonProvider.class);
        c.add(LinkMgtService.class);
		  c.add(ISPMgtService.class);
        classes = Collections.unmodifiableSet(c);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

}
